import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products/products.component';
import { ServicesComponent } from './services/services.component';
import {MfeOneRoutingModule} from "./mfe-one-routing.module";

@NgModule({
  declarations: [
    ProductsComponent,
    ServicesComponent
  ],
  imports: [
    CommonModule,
    MfeOneRoutingModule
  ]
})
export class MfeOneModule { }
