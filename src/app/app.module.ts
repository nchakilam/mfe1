import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MfeOneModule} from "./mfe-one/mfe-one.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MfeOneModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
